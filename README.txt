About
=====

This adds a new display style to views called "Isotope Layout ". Similar to how you select "HTML List" or "Unformatted List" as display styles.

This module requires Views , Views UI and jQuery Update to be enabled. This provide the views items as isotope items using isotope js by  David DeSandro's Isotope library . It also add some default css for the views items which can be easily overridable.

Usage
=====

Go to Views andUse the display mode as "Isotope Layout"