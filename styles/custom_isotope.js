/*
* Custom js for isotope container
*/


(function($){
	$(document).ready(function(){
		var $isotope_containers= [$('#container')];

		function isotope_function($container , $filters){
			// initialize Isotope
				$container.isotope({
				  // options...
				  resizable: false, // disable normal resizing
				  // set columnWidth to a percentage of container width
				  masonry: { columnWidth: $container.width() / 12 }
				});
					// update columnWidth on window resize
				$(window).smartresize(function(){
					$container.isotope({
					// update columnWidth to a percentage of container width
					masonry: { columnWidth: $container.width() / 12 }
					});
				});

				$container.isotope({
				itemSelector : '.element',
				filter : $filters
			  	});
		}
	   	$.each($isotope_containers, function(index, value) {
	       	//recursively calling isotope_function() for each isotope container object
	       	isotope_function(value , '.elements'); //initially hidding all the object with class 'elements'
		});
		$(window).load(function() {
			$.each($isotope_containers, function(index, value) {
		       	//recursively calling isotope_function() for each isotope container object
		       	if(value){ 
		       	//if the object exists on the page then showing the isotope elements
		       		isotope_function(value , '.element');
		       	}
			});
       	});	
	});
})(jQuery);