<?php
/**
 * @file
 * Isotope style plugin for the Views module.
 */

/**
  * Implements a style type plugin for the Views module.
  */
class isotope_views_views_plugin_style_isotope extends views_plugin_style_summary_unformatted {
  
  function options_form(&$form , &$form_state) {
    parent::options_form($form, $form_state);
    $form[classes] = array(
       '#type' => 'textfield',
       '#title' => t('Add Classes to row elements'),
    );
  }
}
