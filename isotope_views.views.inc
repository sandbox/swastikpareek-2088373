<?php
/**
 * @file
 * Contains core functions for the Views module support.
 * Implements hook_views_plugins().
 *
 * This function annnounces the style plugin for Isotope Module views.
 */
function isotope_views_views_plugins(){
  return array(
    'style' => array(
      // Style plugin for the Isotope Module.
      'isotope_views' => array(
        'title' => t('Isotope Layout'),
        'help' => t('Display the results in Isotope Layout'),
        'handler' => 'isotope_views_views_plugin_style_isotope',
        'theme' => 'isotope_views',
        'theme path' => drupal_get_path('module', 'isotope_views') ,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'even empty' => FALSE,
      ),
    ),
  );
}
